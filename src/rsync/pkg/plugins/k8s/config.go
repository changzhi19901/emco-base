// SPDX-License-Identifier: Apache-2.0
// Copyright (c) 2020 Intel Corporation

package k8s

// For K8s this function is not required to do any operation at this time
func (p *K8sProvider) ApplyConfig(config interface{}) error {
	return nil
}
